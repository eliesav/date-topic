# Date-Topic prediction

Projet dans le cadre du cours Méthodes en apprentissage automatique en master TAL.
Ce projet consiste à prédire un topic dans le temps.
Autrice de ce petit projet expérimental : Elisabeth Savatier.

## Pour un bon environnement de test de ce projet
```
pip install -r requirements.txt
```

## Organisation du git

Au premier niveau : 
- Le rapport au format pdf (`Rapport.pdf`).
- Le corpus traité dans ce projet (`corpus.xml`).
- Le notebook du projet(`projet.ipynb`).
- Le `requirements.txt` permettant l'installation d'un bon environnement virtuel.

Dans le dossier `a_load` :
- Tous les fichiers binaires permettant de ne pas forcément lancer les longs traitements présents dans le notebook.

Dans le dossier `visualiser_lda` :
- Les fichiers html permettant de visualiser plus en détail les topics extraits dans chacune des expériences d'extractions de topics.